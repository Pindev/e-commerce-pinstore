<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','type','price','description','slug','quantity'];

    public function galleries(){
        return $this->hasMany(ProductGallery::class,'products_id');
    }

    public function details(){
        return $this->hasMany(TransactionDetail::class,'products_id');
    }
}
