<?php

namespace App\Http\Controllers\API;

use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function all(Request $request){
        $id = $request->input('id');
        $slug = $request->input('slug');
        $limit = $request->input('limit',6);
        $name = $request->input('name');
        $type = $request->input('type');
        $price_from = $request->input('price_from');
        $price_to = $request->input('price_to');

        if($id){
            $product_id = Product::with('galleries')->find($id);
    
            if($product_id)
                return ResponseFormatter::success($product_id,'Data product berhasil di ambil');
            else
                return ResponseFormatter::error(null,'Data product tidak ada',404);
        }

        if($slug){
            $product_slug = Product::with('galleries')->where('slug',$slug)->first();

            if($product_slug)
                return ResponseFormatter::success($product_slug,'Data product berhasil di ambil');
            else
                return ResponseFormatter::error(null,'Data product tidak ada',404);
        }

        $product = Product::with('galleries');

        if($name){
            $product->where('name','like','%'.$name.'%');
        }
        if($type){
            $product->where('type','like','%'.$type.'%');
        }
        if($price_from){
            $product->where('price','>=',$price_from);
        }
        if($price_to){
            $product->where('price','<=',$price_to);
        }

        return ResponseFormatter::success(
            $product->paginate($limit),
            'Data berhasil diambil'
        );


    }
}
