<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Product;
use App\ProductGallery;
use App\Http\Requests\ProductRequest;
class ProductController extends Controller
{
       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $products = Product::all();
        return view('pages.products.index',compact('products'));
    }

    public function create(){
        return view('pages.products.create');
    }

    public function store(ProductRequest $request){
        $data = $request->all();
        $data['slug'] = Str::slug($request->name, '-');

        Product::create($data);

        return redirect()->route('products.index');
    }

    public function edit($id){
        $product = Product::findOrFail($id);
        return view('pages.products.edit',compact('product'));
    }

    public function destroy($id){
        $products = Product::findOrFail($id)->delete();
        $gallery = ProductGallery::where('products_id',$id)->delete();
        return redirect()->route('products.index');
    }

    public function update(ProductRequest $request, $id){
        $data = Product::findOrFail($id);
        $data['slug'] = Str::slug($request->name, '-');

        $data->update($request->all());

        return redirect()->route('products.index');
    }
    public function gallery($id){
        $items = ProductGallery::with('product')->where('products_id',$id)->get();
        return view('pages.products.gallery',compact('items'));
    }
}
