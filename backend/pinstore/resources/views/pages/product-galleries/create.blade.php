@extends('layouts.default')
@section('title','Tambah Foto Barang')
@section('content')

   <div class="card">
       <div class="card-header"><strong>Tambah Foto Barang</strong></div>
   </div>
   <div class="card-body card-block">
       <form action="{{ route('product-galleries.store') }}" method="POST" enctype="multipart/form-data">
           @csrf
           <div class="form-group">
               <label for="name" class="form-control-label">Nama Barang</label>
               <select name="products_id" id="name" class="form-control @error('products_id') is-invalid @enderror">
                   @foreach ($products as $product)
                       <option value="{{ $product->id }}">{{ $product->name }}</option>
                   @endforeach
               </select>
               @error('products_id')
                   <div class="text-muted text-danger">{{ $message }}</div>
               @enderror
           </div>

           <div class="form-group">
                <label for="photo" class="form-control-label">Foto Barang</label>
                <input type="file" accept="image/*" id="photo" name="photo" value="{{ old('photo') }}" class="form-control @error('photo') is-invalid @enderror">
                @error('photo')
                   <div class="text-muted text-danger">{{ $message }}</div>
               @enderror
            </div>

            <div class="form-group">
                <label for="price" class="form-control-label">Default Barang</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="is_default" id="radio1" value="1" checked>
                <label class="form-check-label" for="radio1">
                    Ya
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="is_default" id="radio2" value="0">
                <label class="form-check-label" for="radio2">
                    Tidak
                </label>
              </div>
              @error('is_default')
                   <div class="text-muted text-danger">{{ $message }}</div>
               @enderror
            </div>



        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Tambah Barang</button>
        </div>
       </form>
   </div>

@endsection