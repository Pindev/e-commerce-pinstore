@extends('layouts.default')
@section('title','Data Barang')
@section('content')

<div class="orders">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="box-title">Foto Barang</div>
                </div>
                <div class="card-body">
                    <div class="table-stats order-table ov-h">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nama Barang</td>
                                    <td>Foto</td>
                                    <td>Default</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                    <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->product->name }}</td>
                                    <td>
                                        <img src="{{ url($item->photo) }}" alt="product gallery pinstore">
                                    </td>
                                    <td>{{ $item->is_default ? 'Ya' : 'Tidak'}}</td>
                                    <td>
                                        <form action="{{ route('product-galleries.destroy', $item->id) }}" class="d-inline" method="POST">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                    @empty
                                        <td>
                                            <td colspan="5" class="text-center"><strong>Data yang anda cari tidak ada</strong></td>
                                        </td>
                                    </tr>
                                    @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection